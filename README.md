## Plugin url : https://atom.io/packages/laravel-snippets-5

## Snippets

* Auth
* Cache
* Console
* Config
* Cookie
* Crypt
* DB
* Eloquent
* Event
* Hash
* Helper
* Input
* Log
* Mail
* Redirect
* Relation
* Request
* Response
* Route
* Schema
* Session
* Storage
* View
* Validator
